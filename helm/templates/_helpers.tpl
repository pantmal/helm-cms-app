{{/*
Expand the name of the chart.
*/}}
{{- define "cms.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "cms.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "cms.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create MariaDB name and version as used by the chart label.
*/}}
{{- define "cms.mariadb.fullname" -}}
{{- printf "%s-%s" (include "cms.fullname" .) .Values.mariadb.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create PhpMyAdmin name and version as used by the chart label.
*/}}
{{- define "cms.phpmyadmin.fullname" -}}
{{- printf "%s-%s" (include "cms.fullname" .) .Values.phpmyadmin.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create Wordpress name and version as used by the chart label.
*/}}
{{- define "cms.wordpress.fullname" -}}
{{- printf "%s-%s" (include "cms.fullname" .) .Values.wordpress.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Create connection string for MariaDB.
*/}} 
{{- define "cms.mariadb.url" -}}
{{- printf "%s-mariadb.%s.svc.cluster.local" (include "cms.fullname" .) .Release.Namespace -}}
{{- end -}}


{{/*
Common labels
*/}}
{{- define "cms.labels" -}}
helm.sh/chart: {{ include "cms.chart" . }}
{{ include "cms.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "cms.selectorLabels" -}}
app.kubernetes.io/name: {{ include "cms.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "cms.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "cms.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
